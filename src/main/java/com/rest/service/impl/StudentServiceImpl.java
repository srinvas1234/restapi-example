package com.rest.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rest.entity.Student;
import com.rest.repository.StudentRepository;
import com.rest.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {
	@Autowired
	private StudentRepository studentRepository;
	@Transactional
	public Student createStudent(Student student) {
			return studentRepository.save(student);
	}
@Transactional(readOnly = true)
public List<Student> getAllStudent() {
		return studentRepository.findAll();
	}
@Transactional
public Student updateStudent(long id, Student student) {
	student.setId(id);
	return studentRepository.save(student);
}
@Transactional
public String deleteStudent(long id) {
	Optional<Student> st=studentRepository.findById(id);
	if(st.get() != null) {
	studentRepository.delete(st.get());
	return "Deleted successfully";
	}
	return "Student not availble "+id+" id ";
}
}